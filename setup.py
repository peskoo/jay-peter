from setuptools import find_packages, setup

INSTALL_REQUIRES = [
    "confz==1.8.1",
    "fastapi==0.95.0",
    "openai==0.27.2",
    "uvicorn[standard]==0.21.1",
    "webexteamssdk==1.6.1",
]

VERSION = "0.0.0"


setup(
    name="jaypeter",
    version=VERSION,
    packages=find_packages(),
    url="https://lasalledutemps.fr",
    author="Pesko",
    author_email="pesko@lasalledutemps.fr",
    description="I know everything.",
    install_requires=INSTALL_REQUIRES,
    entry_points={
        "console_scripts": [
            "jay-init-db=jaypeter.db:main",
            "jay-server=jaypeter.main:main",
        ]
    },
    extras_require={
        "test": [
            "flake8>=4.0.1",
            "mypy>=0.971",
            "pre-commit>=2.20.0",
            "pytest>=7.1.2",
            "pytest-cov>=3.0.0",
        ]
    },
)
