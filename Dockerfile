FROM python:alpine

WORKDIR /jaypeter
COPY . .

RUN pip install --editable . 

CMD ["python", "jaypeter/main.py"]
