from dataclasses import dataclass
import logging

import openai

from jaypeter.config import Config
from jaypeter.db import write_data, read_datas
from jaypeter.schemas.openai import ResponseGPT

logger = logging.getLogger(__name__)
MAX_TOKENS = Config().openai.max_tokens
MODEL = Config().openai.model


@dataclass
class ChatGPT:
    def __post_init__(self):
        logger.info("Instance loaded correctly.")

        self.messages = read_datas()
        openai.api_key = Config().openai.api_key

    def handle_message(self, content: str) -> ResponseGPT:
        """We send our request to openAI ChatGPT."""
        logger.info("Message content sent to openAI API : %s", content)
        self.messages.append({"role": "user", "content": content})

        return openai.ChatCompletion.create(
            model=MODEL,
            max_tokens=MAX_TOKENS,
            messages=self.messages,
        )

    def get_message_content(self, response: ResponseGPT) -> str:
        """Get message content from ResponseGPT Object."""
        chat_response = response.choices[0].message.content
        self.messages.append({"role": "assistant", "content": chat_response})
        write_data(self.messages)

        logger.info("Total tokens : %s", response.usage.total_tokens)
        return chat_response
