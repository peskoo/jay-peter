import logging
import re
from dataclasses import dataclass
from typing import Any

from webexteamssdk import WebexTeamsAPI

from jaypeter.config import Config
from jaypeter.schemas.webex import _WebexMessage

logger = logging.getLogger(__name__)
WEBEX_ACCESS_TOKEN = Config().webex.api_key


@dataclass
class WebexAPI:
    def __post_init__(self):
        self.api = WebexTeamsAPI(access_token=WEBEX_ACCESS_TOKEN)
        self.set_room_infos()

    def set_room_infos(self) -> None:
        self.room_name = Config().webex.room_name
        rooms = self.api.rooms.list()
        for room in rooms:
            if self.room_name in room.title:
                self.room_id = room.id
                break

    def send_message(self, text: str):
        """Send message to the room.

        Args:
            text: The text you wanna send in markdown.

        Return:
            None
        """
        logger.info("Response: %s, RoomId: %s", text, self.room_id)
        self.api.messages.create(roomId=self.room_id, markdown=text)


@dataclass
class WebexMessage(WebexAPI):
    message_details: dict[str, Any]

    def get_message_text(self) -> str:
        return self.message_details.text

    def clean_message_content(self, content: str) -> str:
        """For each message where we are mentionning the bot Jay.

        We have in our message_details.text Jay at the beginning of our sentence.
        So the regex is here to remove it.

        Args:
            content: Strings you wanna clean.

        Return:
            A clean message without the first word (Jay)
        """
        return re.sub(r"^\W*\w+\W*", "", content)


@dataclass
class WebexBot(WebexAPI):
    person_id: str

    def handle_receive_message(self, person_id: str) -> bool:
        """Check if the messages comes from someone else than the bot."""
        if person_id != self.person_id:
            return True
        else:
            logger.info("We do not handle the message from our bot.")

    def is_mentionned(self, mention: str) -> bool:
        if mention and self.person_id in mention:
            return True
        else:
            logger.info("This message contain no mention.")
