import logging
import re
from typing import Union, Any, Optional

import uvicorn
from fastapi import FastAPI

from jaypeter.config import Config
from jaypeter.services.webex import WebexAPI, WebexBot, WebexMessage
from jaypeter.services.openai import ChatGPT, ResponseGPT
from jaypeter.schemas.webex import _WebexMessage

logging.basicConfig(
    encoding="utf-8",
    level=logging.INFO,
)
logger = logging.getLogger(__name__)

APP = FastAPI()
WEBEX = WebexAPI()


@APP.post("/message")
def read_message_from_webex(message: _WebexMessage):
    logger.debug("New message from webex !")

    try:
        jay = WebexBot(person_id=WebexAPI().api.people.me().id)
        # Check if it is not a message from the bot.
        if jay.handle_receive_message(person_id=message.data.personId):

            # Retrieve all informations from the destinataire.
            message_obj = WebexMessage(
                message_details=WEBEX.api.messages.get(messageId=message.data.id)
            )
            message_text = message_obj.get_message_text()

            # On envoit un message que si Jay Peter est cité
            if jay.is_mentionned(mention=message_obj.message_details.mentionedPeople):
                logger.info("Received message from Webex : %s", message_text)

                # Send message to Chat GPT
                if Config().openai.activate:
                    chat_gpt = ChatGPT()
                    response: ResponseGPT = chat_gpt.handle_message(
                        content=message_obj.clean_message_content(content=message_text)
                    )

                    # Sending the bot response to webex
                    WEBEX.send_message(
                        text=chat_gpt.get_message_content(response),
                    )
                else:
                    logger.warning("OpenAI ChatGPT is disabled.")

    except Exception as e:
        raise (e)


def main():
    logger.info("Opening uvicorn to listen messages from webex.")
    uvicorn.run(APP, host="0.0.0.0", port=5000, log_level="info")


if __name__ == "__main__":
    main()
