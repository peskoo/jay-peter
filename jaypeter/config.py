from pathlib import Path

from confz import ConfZ, ConfZFileSource

PROJECT_DIR = Path(__file__).parent.parent.resolve()


class OpenAIConfig(ConfZ):
    activate: bool
    api_key: str
    max_tokens: int
    model: str


class WebexConfig(ConfZ):
    api_key: str
    room_name: str


class Config(ConfZ):
    database: str
    webex: WebexConfig
    openai: OpenAIConfig

    CONFIG_SOURCES = ConfZFileSource(file=PROJECT_DIR / "config.yml")
