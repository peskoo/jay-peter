from typing import Optional

from pydantic import BaseModel


class DataObject(BaseModel):
    id: Optional[str] = None
    roomId: Optional[str] = None
    personId: Optional[str] = None
    personEmail: Optional[str] = None
    created: Optional[str] = None


class _WebexMessage(BaseModel):
    id: Optional[str] = None
    name: Optional[str] = None
    ressource: Optional[str] = None
    event: Optional[str] = None
    filter: Optional[str] = None
    orgId: Optional[str] = None
    createBy: Optional[str] = None
    appId: Optional[str] = None
    ownedBy: Optional[str] = None
    status: Optional[str] = None
    actorId: Optional[str] = None
    data: Optional[DataObject] = None
