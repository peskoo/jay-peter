from dataclasses import dataclass

response_model = {
    "id": "chatcmpl-6p9XYPYSTTRi0xEviKjjilqrWU2Ve",
    "object": "chat.completion",
    "created": 1677649420,
    "model": "gpt-3.5-turbo",
    "usage": {"prompt_tokens": 56, "completion_tokens": 31, "total_tokens": 87},
    "choices": [
        {
            "message": {
                "role": "assistant",
                "content": "The 2020 World Series was played in Arlington, Texas at the Globe Life Field, which was the new home stadium for the Texas Rangers.",
            },
            "finish_reason": "stop",
            "index": 0,
        }
    ],
}


# Create a file with the conversation
@dataclass
class Usage:
    prompt_tokens: int
    completion_tokens: int
    total_tokens: int


@dataclass
class Message:
    role: str
    content: str


@dataclass
class Choices:
    message: dict[Message]
    finish_reason: str
    index: int


@dataclass
class ResponseGPT:
    id: str
    object: str
    created: int
    model: str
    usage: dict[Usage]
    choices: list[Choices]
