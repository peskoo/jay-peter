"""Handle prompt history.

If we save prompt history, chat GPT will be able to keep history of our conversations.
But eache word is a token, and the pricing is done by tokens sent to chat GPT.

So we must keep a limit to avoid a huge bill by the end of the month.

This limit can be set when we set our ChatCompletion model in services/openai.py L.86.
"""
import logging
import json
from pathlib import Path

from jaypeter.config import Config

PROJECT_DIR = Path(__file__).parent.parent.resolve()
DATABASE = PROJECT_DIR / Config().database
logger = logging.getLogger(__name__)


INT_MESSAGE = {
    "role": "system",
    "content": "You are a coding tutor bot to help user write and optimize python code.",
}


def write_data(data: dict[str, str]):
    with open(DATABASE, "w") as file:
        logger.debug("WRITE : %s", file)
        json.dump(data, file)


def read_datas() -> list[dict[str, str]]:
    with open(DATABASE, "r") as file:
        logger.debug("READ : %s", file)
        data = json.load(file)

    if isinstance(data, dict):
        return [data]
    elif isinstance(data, list):
        return data
    else:
        logger.error(
            "Data type are not valid : %s ; We handle list or dict.", type(data)
        )


def init_db():
    write_data(INT_MESSAGE)
    logger.info("Database reset.")


def main():
    print("REBOOT DATABASE")
    init_db()
    print("DONE.")


if __name__ == "__main__":
    main()
