# Bot Webex

## Configuration
You can juste rename config-example.yml to config.yml and add your api keys directly in it.
```yml
# config.yml

database: database.json
webex:
  api_key: WEBEX ACCESS KEY
  room_name: "Best Team Ever"
openai:
  activate: true
  api_key: OPENAI API KEY
  max_tokens: 1500
```
database: you can juste rename database-example.json by database.json  
The path to your file starts from root project directory.

room_name: add the room name where your bot is.

activate: if false, the webex webhook will not hit openAI. If you wanna make some
tokens saving.

max_tokens: this is the maximum tokens you wanna send to openAI, be careful with this
limit because if it too high you may hit your api limit to fast.  
Do not forget to set your usage limit
[here](https://platform.openai.com/account/billing/limits)

## Database

There is a special entrypoint to init database: `jay-init-db`  
As for the max_tokens limit, this is everything about your budget gestion.  
You can set up a cron to every 30 minutes or hour to execute the command and it will
reset your database correctly without forget to reset the system prompt.


## Server
Start web server with `jay-server` command.

## Dockerfile
```bash
$ docker build -t jay-peter-img .
$ docker run -d --name jay-peter-ct -p 5000:5000 jay-peter-img
```
